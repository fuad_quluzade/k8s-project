package az.ingress;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8sProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(K8sProjectApplication.class, args);
    }

}
